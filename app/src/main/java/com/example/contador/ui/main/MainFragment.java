package com.example.contador.ui.main;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.contador.R;
import com.google.android.material.snackbar.Snackbar;

import java.text.CollationElementIterator;

public class MainFragment extends Fragment {

    private MainViewModel mViewModel;
    private View view;
    private int l1 = 20;
    private int p1 = 0;
    private int l2 = 20;
    private int p2 = 0;
    private TextView contador1;
    private TextView contador2;
    private ImageButton p1ll;
    private ImageButton p1lm;
    private Button p1pm;
    private Button p1pl;
    private ImageButton p2ll;
    private ImageButton p2lm;
    private Button p2pm;
    private Button p2pl;
    private ImageButton p22p1;
    private ImageButton p12p2;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.main_fragment, container, false);

        p1ll = view.findViewById(R.id.p1ll);
        p1lm = view.findViewById(R.id.p1lm);
        p1pm = view.findViewById(R.id.p1pm);
        p1pl = view.findViewById(R.id.p1pl);
        p2ll = view.findViewById(R.id.p2ll);
        p2lm = view.findViewById(R.id.p2lm);
        p2pm = view.findViewById(R.id.p2pm);
        p2pl = view.findViewById(R.id.p2pl);
        p22p1 = view.findViewById(R.id.p22p1);
        p12p2 = view.findViewById(R.id.p12p2);
        contador1 = view.findViewById(R.id.contador1);
        contador2 = view.findViewById(R.id.contador2);

        if (savedInstanceState != null){
            l1 = savedInstanceState.getInt("l1");
            l2 = savedInstanceState.getInt("l2");
            p1 = savedInstanceState.getInt("p1");
            p2 = savedInstanceState.getInt("p2");

            updateViews();
        }

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (view.getId()){
                    case R.id.p1ll:
                        if (l1 > 0) {
                            l1--;
                            updateContador(1);
                        }
                        break;
                    case R.id.p1lm:
                        l1++;
                        updateContador(1);
                        break;
                    case R.id.p1pm:
                        p1++;
                        updateContador(1);
                        break;
                    case R.id.p1pl:
                        if (p1 > 0) {
                            p1--;
                            updateContador(1);
                        }
                        break;
                    case R.id.p2ll:
                        if (l2 > 0) {
                            l2--;
                            updateContador(2);
                        }
                    case R.id.p2lm:
                        l2++;
                        updateContador(2);
                        break;
                    case R.id.p2pm:
                        p2++;
                        updateContador(2);
                        break;
                    case R.id.p2pl:
                        if (p2 > 0) {
                            p2--;
                            updateContador(2);
                        }
                        break;
                    case R.id.p22p1:
                        if (l2 > 0){
                            l2--;
                            l1++;
                            updateContador(1);
                            updateContador(2);

                        }
                        break;
                    case R.id.p12p2:
                        if (l1 > 0){
                            l1--;
                            l2++;
                            updateContador(1);
                            updateContador(2);

                        }
                        break;
                }
                updateViews();

            }
        };


        p1ll.setOnClickListener(listener);
        p1lm.setOnClickListener(listener);
        p1pm.setOnClickListener(listener);
        p1pl.setOnClickListener(listener);
        p2ll.setOnClickListener(listener);
        p2lm.setOnClickListener(listener);
        p2pm.setOnClickListener(listener);
        p2pl.setOnClickListener(listener);
        p22p1.setOnClickListener(listener);
        p12p2.setOnClickListener(listener);
        contador1.setOnClickListener(listener);
        contador2.setOnClickListener(listener);

        reset();

        return view;
    }

    private void reset(){
        l1 = 20;
        l2 = 20;
        p2 = 0;
        p1 = 0;

        updateViews();
    }

    private void updateViews() {
        contador1.setText(String.format("%d/%d", l1, p1));
        contador2.setText(String.format("%d/%d", l2, p2));
    }


    //UPDATE CONTADORES

    private void updateContador(int ncontador) {
        switch (ncontador) {
            case 1:
                System.out.println("axq");
                this.contador1.setText(String.format("%d/%d", l1, p1));
                updateViews();

                break;
            case 2:
                System.out.println("asd");
                this.contador2.setText(String.format("%d/%d", l2, p2));
                updateViews();

        }

    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        // TODO: Use the ViewModel
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.menu_contador_fragment, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.reset) {
            reset();
            Snackbar.make(view, "New Game!", Snackbar.LENGTH_LONG).show();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt("l1", l1);
        outState.putInt("l2", l2);
        outState.putInt("p1", p1);
        outState.putInt("p2", p2);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }
}
